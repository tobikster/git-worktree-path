# git-worktree-path

Converts between **relative / absolute worktree paths**. Handles bare and normal repos. Tested with `git 2.29`; probably works with any version since ~2017.

Also available:
- `git worktree-path -a git CMD` applies `CMD` (e.g. `pull` / `push`) in all non-main worktrees
- `git worktree-path -a cmd CMD` similarly runs an arbitrary (non-git) command (including `sh -c '...' sh a1 a2 ...`)
- `git worktree-orphan /tmp/unrelated` creates an orphan branch / worktree with an initial empty commit

Why would you do this? Relativized worktrees don't break when you access them via a different path, for example in a **chroot**, or if you move a repository with **embedded worktrees** (which is a particularly convenient workflow).

## Copyright

`Alin Mr <almr.oss@outlook.com>` / MIT license

## Usage

Install in `$PATH`, then call from within worktree or repo, or via `git -C`

```
git-worktree-path [options] { status | abs | rel | remove } [PATH]
git-worktree-path [-v]      move FROM TO
git-worktree-path [options] { git[cmd] | cmd } PATH-UNLESS--A CMD [ARG...]

Options:
  -h, --help
  -v, --verbose
  -a, --all        for all worktrees (rel | abs | [re]move: non-main)
  -A               like -a, but don't stop on errors
  -b, --branch BR  worktree for BR (':' = main worktree)
  --detect-gitdir  print git main & common dir (-a/-b: worktree dirs too)
  --keep-symlink   preserve symlink path components as much as possible
  --no-main        ignore main worktree

PATH is forbidden for '-a'; without -a, *cmd REQUIRES it ('' = current WT).
Call from a worktree, (possibly bare) repo, or via 'git -C' or '$GIT_DIR'.
```

## Limitations

- `git worktree`-specific commands (e.g. `move`, `remove`) will refuse to work with relativized worktrees; you must convert to absolute paths first. Use `worktree-path { move | remove } ...` to automatically convert.
- `git worktree prune`, if run *from within a relativized worktree*, will destroy the repository's link to the worktree. This is because because all worktree paths get resolved relative to the current `gitdir`, which in a worktree is not the repo's `gitdir`. In normal repos, worktree paths are absolute, so path resolution is moot. **Avoid this!** If it does happen, a workaround would be to `worktree add` the commit-ish associated with the worktree somewhere else, then carefully rsync the disconnected worktree in and remove it.

## Related

- `git worktree repair`: as long as `/path/to/WT/.git` points to the right repo, will re-link worktree (absolute path) into `repo/.git/worktrees/WT/gitdir`
- `git worktree prune`: doesn't remove relativized worktrees, which proves correctness (but see limitations above)

## Other efforts

- [fix-git-worktree](https://github.com/harobed/fix-git-worktree) based on a 2016 feature request and [discussion on Git mailing list](https://public-inbox.org/git/CACsJy8AZVWNQNgsw21EF2UOk42oFeyHSRntw_rpeZz_OT1xdMw@mail.gmail.com/T/)
- [git-worktree-relative](https://github.com/Kristian-Tan/git-worktree-relative/)

Compared to these, I've tried to use pure `bash`, as few `git` internals as possible (most details are gathered via `git rev-parse`), and as little code as possible.
